#!/bin/sh

init_binfmt_service_name=init_binfmt.service
init_binfmt_script=/root/init_binfmt
init_binfmt_service=/etc/systemd/system/$init_binfmt_service_name

cat > $init_binfmt_script << EOF
#!/bin/sh

set -xe

/sbin/modprobe binfmt_misc

mount -t binfmt_misc binfmt_misc /proc/sys/fs/binfmt_misc

# Support for ARM binaries through Qemu:
{ echo ':arm:M::\x7fELF\x01\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\x28\x00:\xff\xff\xff\xff\xff\xff\xff\x00\xff\xff\xff\xff\xff\xff\xff\xff\xfe\xff\xff\xff:/usr/bin/qemu-arm-static:' > /proc/sys/fs/binfmt_misc/register; } 2>/dev/null
{ echo ':armeb:M::\x7fELF\x01\x02\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\x28:\xff\xff\xff\xff\xff\xff\xff\x00\xff\xff\xff\xff\xff\xff\xff\xff\xff\xfe\xff\xff:/usr/bin/qemu-armeb-static:' > /proc/sys/fs/binfmt_misc/register; } 2>/dev/null
EOF

chmod +x $init_binfmt_script

cat > $init_binfmt_service << EOF
[Unit]
Description=Init binfmt

[Service]
Type=oneshot
ExecStart=$init_binfmt_script
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl start $init_binfmt_service_name

dd if=/dev/zero of=/swap.img bs=1M count=2048
mkswap /swap.img
chmod 0600 /swap.img
swapon /swap.img

cat <<EOF > /etc/systemd/system/early-docker.target
[Unit]
Description=Run Docker containers before main Docker starts up
EOF

