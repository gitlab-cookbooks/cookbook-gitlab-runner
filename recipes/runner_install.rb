#
# Cookbook Name:: cookbook-gitlab-runner
# Recipe:: runner_install
#
# Copyright 2016, GitLab Inc.
#

case node['platform_family']
when 'debian'
  apt_repository_with_key_expiry node['cookbook-gitlab-runner']['gitlab-runner']['repository'] do
    uri "https://packages.gitlab.com/runner/#{node['cookbook-gitlab-runner']['gitlab-runner']['repository']}/#{node['platform']}"
    components ['main']
    distribution node['lsb']['codename']
    key "https://packages.gitlab.com/runner/#{node['cookbook-gitlab-runner']['gitlab-runner']['repository']}/gpgkey"
  end

  if node['cookbook-gitlab-runner']['gitlab-runner']['install_helper_images_package']
    package node['cookbook-gitlab-runner']['gitlab-runner']['helper_images_package'] do
      version node['cookbook-gitlab-runner']['gitlab-runner']['version']
      options '--allow-change-held-packages'
    end
  end

  package node['cookbook-gitlab-runner']['gitlab-runner']['package'] do
    version node['cookbook-gitlab-runner']['gitlab-runner']['version']
    options '--allow-change-held-packages'
  end

when 'rhel'
  yum_repository node['cookbook-gitlab-runner']['gitlab-runner']['repository'] do
    description 'GitLab Runner package server'
    baseurl "https://packages.gitlab.com/runner/#{node['cookbook-gitlab-runner']['gitlab-runner']['repository']}/el/#{node['platform_version'][0]}/$basearch"
    gpgkey "https://packages.gitlab.com/runner/#{node['cookbook-gitlab-runner']['gitlab-runner']['repository']}/gpgkey"
    gpgcheck false # TODO, change when individual packages get signed
  end

  if node['cookbook-gitlab-runner']['gitlab-runner']['install_helper_images_package']
    package node['cookbook-gitlab-runner']['gitlab-runner']['helper_images_package'] do
      version node['cookbook-gitlab-runner']['gitlab-runner']['version']
      allow_downgrade true
    end
  end

  package node['cookbook-gitlab-runner']['gitlab-runner']['package'] do
    version node['cookbook-gitlab-runner']['gitlab-runner']['version']
    allow_downgrade true
  end
end

package 'git'
