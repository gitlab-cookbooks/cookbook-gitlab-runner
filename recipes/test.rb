# We use older version of chef for compatibility with our infra setup.
# However, this version of chef package in Kitchen tests comes with older
# SSL certificates bundle. This already causes problems with verifying
# certs, for example the Let's Encrypt provisioned certificate for
# packages.gitlab.com. Linking to system SSL certs solves that problem.
#
# This recipe is not meant to be used in production setups!
#

raise "This recipe is only for tests! Not for production usage!" unless ENV["TEST_KITCHEN"]

link '/opt/chef' do
  to '/opt/cinc'
  only_if { ::File.directory? '/opt/cinc' }
end

link '/opt/chef/embedded/ssl/cert.pem' do
  action :create
  link_type :symbolic
  to '/etc/ssl/certs/ca-certificates.crt'
end
