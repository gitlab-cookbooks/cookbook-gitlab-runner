#
# Cookbook Name:: cookbook-gitlab-runner
# Recipe:: fleeting plugin install
#
# Copyright 2023, GitLab Inc.
#

plugin_name = node['cookbook-gitlab-runner']['fleeting-plugin']['plugin_name']
version = node['cookbook-gitlab-runner']['fleeting-plugin']['version']
file_name = node['cookbook-gitlab-runner']['fleeting-plugin']['file_name']
checksum = node['cookbook-gitlab-runner']['fleeting-plugin']['checksum']

remote_file "/usr/local/bin/#{plugin_name}" do
  source "https://gitlab.com/gitlab-org/fleeting/#{plugin_name}/-/releases/#{version}/downloads/#{file_name}"
  owner 'root'
  group 'root'
  mode '0700'
  checksum checksum
  only_if { node['cookbook-gitlab-runner']['fleeting-plugin']['install'] }
end
