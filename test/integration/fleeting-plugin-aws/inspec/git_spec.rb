control "Git" do
  title "installation"
  describe package('git') do
    it { should be_installed }
  end
end
