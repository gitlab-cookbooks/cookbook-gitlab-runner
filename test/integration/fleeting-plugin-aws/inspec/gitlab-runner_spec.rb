control "GitLab Runner" do
  title "setup and configuration"

  describe package('gitlab-runner') do
    it { should be_installed }
  end

  describe package('gitlab-runner-helper-images') do
    it { should be_installed }
  end

  describe command('gitlab-runner --version') do
    its(:stdout) { should match /Version:\s+17\.8\.2/ }
  end

  describe file('/usr/local/bin/fleeting-plugin-aws') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_mode 0700 }
  end

  describe file('/etc/gitlab-runner/config.toml') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_mode 0600 }
  end

  describe file('/etc/gitlab-runner/config.toml') do
    its(:content) { should match /concurrent = 10/ }
    its(:content) { should match /\[\[runners]]/ }
    its(:content) { should match /name = "test-aws"/ }
    its(:content) { should match /\[runners.autoscaler]/ }
    its(:content) { should match /capacity_per_instance = 1/ }
    its(:content) { should match /max_use_count = 1/ }
    its(:content) { should match /max_instances = 20/ }
    its(:content) { should match /plugin = "fleeting-plugin-aws"/ }
    its(:content) { should match /\[runners.autoscaler.plugin_config]/ }
    its(:content) { should match /project = "test-project"/ }
    its(:content) { should match /region = "test-region"/ }
    its(:content) { should match /name = "test-autoscaling-group"/ }
    its(:content) { should match /\[runners.autoscaler.connector_config]/ }
    its(:content) { should match /username = "ubuntu"/ }
    its(:content) { should match /\[\[runners.autoscaler.policy]]/ }
    its(:content) { should match /idle_count = 1/ }
    its(:content) { should match /idle_time = 0/ }
    its(:content) { should match /scale_factor = 0.0/ }
    its(:content) { should match /scale_factor_limit = 0/ }
  end

  describe file('/etc/gitlab-runner/cloudinit.sh') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_mode 0700 }
  end

  describe file('/etc/gitlab-runner/cloudinit.sh') do
    its(:content) { should match /#!\/bin\/sh/ }
    its(:content) { should match /\/sbin\/modprobe binfmt_misc/ }
    its(:content) { should match /systemctl daemon-reload/ }
  end

  describe systemd_service('gitlab-runner') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end
