class MachineOptionsRewriter
  attr_reader :machine

  def initialize(machine)
    @machine = machine
  end

  def rewrite
    extract_machine_options(machine['MachineOptions']) if machine.key?('MachineOptions')
    extract_machine_options_map(machine['MachineOptionsMap']) if machine.key?('MachineOptionsMap')
    options
  end

  protected

  def extract_machine_options(machine_options)
    options.concat(machine_options)
  end

  def extract_machine_options_map(machine_options_map)
    machine_options_map.each do |key, value|
      case
      when value.is_a?(Array)
        value.each do |v|
          options << "#{key}=#{v}"
        end
      when value.nil?
        options << "#{key}"
      else
        options << "#{key}=#{value}"
      end
    end
  end

  def options
    @options ||= []
  end
end

class RunnersRewriter
  attr_reader :runners

  def initialize(runners)
    @runners = runners.to_hash
  end

  def rewrite
    rewritten_runners = {}

    runners.each do |name, runner|
      rewritten_runners[name] = prepare_runner(runner)
    end

    rewritten_runners
  end

  private

  def prepare_runner(runner)
    runner['machine'] = prepare_runner_machine(runner['machine']) if runner.key?('machine')
    runner
  end

  def prepare_runner_machine(machine)
    options = MachineOptionsRewriter.new(machine).rewrite

    machine.tap {|hs| hs.delete('MachineOptionsMap')} if machine.key?('MachineOptionsMap')
    machine['MachineOptions'] = options
    machine
  end
end
