module QuoteHelper
  def quote(value)
    if value && value.is_a?(String)
      "\"#{value}\""
    else
      value
    end
  end
end
